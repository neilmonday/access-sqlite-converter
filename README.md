Access SQLite Converter
=======================

Intro
=====

The Access SQLite Converter is used to transform Microsoft Access databases (*.mdb, *.accdb) to SQLite databases (*.sqlite).